from django.test import TestCase, Client
from django.urls import resolve
from .views import index, profil
from .models import Message
from .forms import Message_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
# from django.contrib.staticfiles.testing import LiveServerTestCase
import unittest

# from .models import Todo
# from .form import Todo_Form
# # Create your tests here.
class landingTest(TestCase):
    def test_url_landing_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200, "wrong URL")

    def test_url_profil_is_exist(self):
        response = Client().get('/profil/')
        self.assertEqual(response.status_code, 200, "wrong URL")

    def test_using_index_func(self):
        found  = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_using_profil_func(self):
        found  = resolve('/profil/')
        self.assertEqual(found.func, profil)

    def test_model_Message(self):
        # Creating a new activity
        new_activity = Message.objects.create(status='mengerjakan lab ppw')
    
        # Retrieving all available activity
        counting_all_available_Message = Message.objects.all().count()
        self.assertEqual(counting_all_available_Message, 1)

    def test_form_validation_for_blank_items(self):
        form = Message_Form(data={'status': ''})
        self.assertFalse(form.is_valid())
        
    def test_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/', {'status': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_post_error_and_render_the_result(self):
        test = 'REKA GANTENG'
        response_post = Client().post('/', {'status': '' })
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_HAE(self):
        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn("HAE", html_response)
    def test_RekaPaskaEnda(self):
        response = Client().get('/profil/')
        html_response = response.content.decode('utf8')
        self.assertIn("Reka Paska Enda", html_response)
    def test_gambar(self):
        response = Client().get('/profil/')
        url_gambar = "https://instagram.fcgk4-2.fna.fbcdn.net/vp/9f31042bd0657d1a65f9d1ac1b60fb6a/5D25013C/t51.2885-15/e35/41974063_2155705198014302_197054604127074740_n.jpg?_nc_ht=instagram.fcgk4-2.fna.fbcdn.net"
        html_response = response.content.decode('utf8')
        self.assertIn(url_gambar, html_response)
    def test_npm(self):
        response = Client().get('/profil/')
        html_response = response.content.decode('utf8')
        self.assertIn("1806186793", html_response)

class FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        # self.selenium.refresh()
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(4.0)
        # find the form element
        status = selenium.find_element_by_id('id_status')

        submit = selenium.find_element_by_id('id_submit')

        # Fill the form with data
        status.send_keys('Mengerjakan Lab PPW Percobaan Kedua')

        # submitting the form
        submit.click()
        #check
        assert 'Mengerjakan Lab PPW' in selenium.page_source
    def test_profil(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(4.0)
        # find the form element
        profil = selenium.find_element_by_id('id_profil')
        
        # submitting the form
        profil.click()

        nama = selenium.find_element_by_tag_name('h1').text
        self.assertEqual("Reka Paska Enda", nama)
    def test_tema(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(4.0)

        tema = selenium.find_element_by_id('tema')

        tema.click()

        body  = selenium.find_element_by_tag_name('body')

        color = body.value_of_css_property('background-color')
        self.assertEqual('rgba(255, 192, 203, 1)', color)

    def test_accordion(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/profil')
        time.sleep(4.0)
        

        aktivitas = selenium.find_element_by_id('aktivitas')
        aktivitas.click()
        aktif = selenium.find_element_by_id('aktif')
        display = aktif.value_of_css_property('display')
        self.assertEqual('block', display)
    
    #TEST CHALLENGE
    def test_css(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(4.0)
        hae = selenium.find_element_by_class_name('h1')
        style = hae.get_attribute('style')
        self.assertEqual('margin-top: 50px;', style)
    
    def test_css_2(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(4.0)
        background = selenium.find_element_by_tag_name('body')
        color = background.value_of_css_property('background-color')
        self.assertEqual('rgba(255, 255, 255, 1)',color)

    def test_layout(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/profil')
        time.sleep(4.0)
        tombol = selenium.find_element_by_class_name('tombol')
        self.assertIn('tombol',selenium.page_source)
    def test_layout_2(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        time.sleep(4.0)
        status = selenium.find_element_by_id('id_status')
        self.assertIn('status',selenium.page_source)
    


# if __name__ == '__main__':#
#     unittest.main(warnings='ignore')
    