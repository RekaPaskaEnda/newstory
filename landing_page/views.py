from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import Message
from .forms import Message_Form

# Create your views here.
def index(request):
    if request.method == 'POST':
        iniForm = Message_Form(request.POST)
        if iniForm.is_valid():
            iniForm.save()
            return HttpResponseRedirect(reverse(index))

    else :
        iniForm = Message_Form()
    dataBase = Message.objects.all()
    content = {'forms':iniForm, 'data': dataBase}
    return render(request, 'landing_page.html', content)

def profil(request):
    return render(request, 'profil.html')


