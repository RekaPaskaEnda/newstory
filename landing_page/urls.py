from django.urls import path
from .views import index, profil

urlpatterns = [
    path('', index, name='index'),
    path('profil/', profil, name='profil'),
    
]
