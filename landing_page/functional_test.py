from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import TestCase
import unittest




class FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        status = selenium.find_element_by_id('id_status')

        submit = selenium.find_element_by_id('id_submit')

        # Fill the form with data
        status.send_keys('Mengerjakan Lab PPW')

        # submitting the form
        submit.send_keys(Keys.RETURN)
        #check
        assert 'Mengerjakan Lab PPW' in selenium.page_source
    def test_profil(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        profil = selenium.find_element_by_id('id_profil')
        
        # submitting the form
        profil.send_keys(Keys.RETURN)

        nama = selenium.find_element_by_tag_name('h1').text
        self.assertEqual("Reka Paska Enda", nama)

if __name__ == '__main__':#
    unittest.main(warnings='ignore')
