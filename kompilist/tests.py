from django.test import TestCase, Client
from django.urls import resolve
from .views import index, jsonlist
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest

class kompitlist(TestCase):
    def test_url_exists(self):
        response = Client().get('/kompilist/')
        self.assertEqual(response.status_code, 200, "wrong URL")
    def test_url_json_exists(self):
        response = Client().get('/kompilist/jsonlist/')
        self.assertEqual(response.status_code, 200, "wrong URL")
    def test_func(self):
        found  = resolve('/kompilist/')
        self.assertEqual(found.func, index)
    def test_func_json(self):
        found  = resolve('/kompilist/jsonlist/')
        self.assertEqual(found.func, jsonlist)
    def test_something_in_web(self):
        response= Client().get('/kompilist/')
        html_response = response.content.decode('utf8')
        self.assertIn("Harga", html_response)
    

    


# Create your tests here.
