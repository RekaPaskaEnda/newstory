from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.http import JsonResponse
from django.contrib.auth import logout as out
import random
import requests
# from .models import Message
# from .forms import Message_Form

def index(request):
    if request.user.is_authenticated:
        request.session["firstname"] = request.user.first_name
    return render(request,'list.html')
def jsonlist(request):
    lst = []
    response = requests.get("https://enterkomputer.com/api/product/notebook.json")
    data = response.json()
    i = 0
    while i < 100:
        j = 1000
        lst.append(data[j+i])
        i += 1

    return JsonResponse(lst, safe=False)

def logout(request):
    out(request)

    return HttpResponseRedirect("/kompilist/")

# Create your views here.
