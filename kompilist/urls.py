from django.urls import path, include
from .views import index, jsonlist, logout
from django.contrib.auth import login

urlpatterns = [
    path('', index, name='kompilist'),
    path('jsonlist/', jsonlist, name='jsonlist'),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),
]