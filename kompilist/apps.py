from django.apps import AppConfig


class KompilistConfig(AppConfig):
    name = 'kompilist'
