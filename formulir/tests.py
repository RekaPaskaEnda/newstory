from django.test import TestCase, Client
from django.urls import resolve
from .views import daftar, check
from .models import Pendaftaran
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest

class kompitlist(TestCase):
    def test_url_exists(self):
        response = Client().get('/formulir/')
        self.assertEqual(response.status_code, 200, "wrong URL")
    def test_func(self):
        found  = resolve('/formulir/')
        self.assertEqual(found.func, daftar)
    def test_func_json(self):
        found  = resolve('/formulir/check/')
        self.assertEqual(found.func, check)
    def test_something_in_web(self):
        response= Client().get('/formulir/')
        html_response = response.content.decode('utf8')
        self.assertIn("WHAT", html_response)
    def test_model_Message(self):
        # Creating a new activity
        new_activity = Pendaftaran.objects.create(nama='mengerjakan lab ppw',email='lol@email.com',password='qwe')
        # Retrieving all available activity
        counting_all_available_Message = Pendaftaran.objects.all().count()
        self.assertEqual(counting_all_available_Message, 1)
    