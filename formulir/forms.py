from django import forms
from .models import Pendaftaran

class Pendaftaran_Form(forms.ModelForm):
    password = forms.CharField(label='Password:', widget=forms.PasswordInput(attrs={'class':'form-control', 'id':'password'}), required=True)
    nama = forms.CharField(label='Nama:', widget=forms.TextInput(
        attrs={ 'class': 'form-control'}), required=True)
    email = forms.CharField(label='Email:', widget=forms.TextInput(
        attrs={'class': 'form-control email', 'onkeyup' : 'check_email(this);',}), required=True)
    class Meta:
        model = Pendaftaran
        fields = ["nama", 'email', "password"]

# class Diary_Form(forms.ModelForm):
#     tgl = forms.CharField(label='Tanggal:', widget=forms.TextInput(
#         attrs={ 'class': 'form-control'}))
#     catatan = forms.CharField(label='Catatan:', widget=forms.TextInput(
#         attrs={ 'class': 'form-control'}))
#     class Meta:
#         model = Diary
#         fields = ["tgl", "catatan"]
        
