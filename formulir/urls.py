from django.urls import path
from .views import daftar, check

urlpatterns = [
    path('', daftar, name='daftar'),
    path('check/', check, name='check')
]