from django.http import HttpResponseRedirect
from django.contrib import messages
from django.urls import reverse
from django.shortcuts import render
from .models import Pendaftaran
from .forms import Pendaftaran_Form
from django import forms
from django.http import JsonResponse
from django.http import Http404

# Create your views here.
def daftar(request):
    if request.method == 'POST':
        iniForm = Pendaftaran_Form(request.POST)
        if iniForm.is_valid():
                iniForm.save()
                messages.success(request,"Terima Kasih Anda Telah Terdaftar")
                return HttpResponseRedirect(reverse(daftar))
            
    else :
        iniForm = Pendaftaran_Form()

    dataBase = Pendaftaran.objects.all()
    content = {'forms':iniForm, 'data': dataBase}
    return render(request, 'formulir.html', content)


def check(request):
    response = {}
    if request.method == "GET":
        raise HttpResponseRedirect("/daftar/")
    else:
        response["is_exist"] = Pendaftaran.objects.filter(email=request.POST['email']).exists()
        response["is_valid"] = '@' in request.POST['email'] and '.com' in request.POST['email']
    return JsonResponse(response)

# def check(request):
#     response = {}
#     emails = request.POST['email']
#     obj = Pendaftaran.objects.all()

#     if request.method == "GET":
#         raise Http404("URL doesn't exists")
#     else:
#         if emails in obj:
#             response["exist"] = True
#             if ('@' in emails and '.com' in emails) == True:
#                 response["valid"] = True
#             else:
#                 response["valid"] = False
#         else:
#             response["exist"] = False
#             if ('@' in emails and '.com' in emails) == True:
#                 response["valid"] = True
#             else:
#                 response["valid"] = False
        
#     return JsonResponse(response)


    # json = JsonResponse({"nama": nama,"email": email,"password": password}, safe=False)
    # return json

# def daftar_diary(request):
#     if request.method == 'POST':
#         iniForm = Diary_Form(request.POST)
#         if iniForm.is_valid():
#                 iniForm.save()
#                 messages.success(request,"Terima Kasih Anda Telah Terdaftar")
#                 return HttpResponseRedirect(reverse(daftar))
            
#     else :
#         iniForm = Diary_Form()

#     dataBase = Pendaftaran.objects.all()
#     content = {'diary':iniForm, 'data': dataBase}
#     return render(request, 'formulir.html', content)


# def diaryulasi(request):
#     tgl = request.GET('tgl')
#     catatan = request.GET('catatan')
#     json = JsonResponse({"tanggal": tgl,"catatan": catatan}, safe=False)
#     return json

# def diaru(request):
#     tgl = request.GET.get('tgl')
